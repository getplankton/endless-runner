﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;

#if UNITY_ANALYTICS
using UnityEngine.Analytics;
#endif

public class ShopThemeList : ShopList
{
    public override void Populate()
    {
		m_RefreshCallback = null;
        foreach (Transform t in listRoot)
        {
            Destroy(t.gameObject);
        }

        foreach (KeyValuePair<string, ThemeData> pair in ThemeDatabase.dictionnary)
        {
            ThemeData theme = pair.Value;
            if (theme != null)
            {
                prefabItem.InstantiateAsync().Completed += (op) =>
                {
                    if (op.Result == null || !(op.Result is GameObject))
                    {
                        Debug.LogWarning(string.Format("Unable to load theme shop list {0}.", prefabItem.Asset.name));
                        return;
                    }
                    GameObject newEntry = op.Result;
                    newEntry.transform.SetParent(listRoot, false);

                    ShopItemListItem itm = newEntry.GetComponent<ShopItemListItem>();

                    itm.nameText.text = theme.themeName;
                    
                    itm.icon.sprite = theme.themeIcon;


                    itm.buyButton.onClick.AddListener(delegate() { Buy(theme); });

                    itm.buyButton.image.sprite = itm.buyButtonSprite;

                    RefreshButton(itm, theme);
                    m_RefreshCallback += delegate() { RefreshButton(itm, theme); };
                };
            }
        }
    }

	protected void RefreshButton(ShopItemListItem itm, ThemeData theme)
	{
		if (PlayerData.instance.themes.Contains(theme.themeName))
		{
			itm.buyButton.interactable = false;
			itm.buyButton.image.sprite = itm.disabledButtonSprite;
			itm.buyButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Owned";
		}
	}


	public void Buy(ThemeData t)
    {
        PlayerData.instance.AddTheme(t.themeName);
        PlayerData.instance.Save();

        // Repopulate to change button accordingly.
        Populate();
    }
}
